Roadmap:
TO DO
-----

- finish review process to promote module out of sandbox.
- Bug fixes?
- Better styling
- Bootstrap option ?
- Release!

WON'T DO
--------
Considered these but decided against them.

- Add option to AJAX refresh to check if next link should be enabled?

DONE
----
Version 7x-0.1.0:

- Configurable display of first/last links
- Start process to switch from sandbox->full project.

Version 7x-0.2.0:

- Hide prev/first links if already at first log entry
- Hide next/last links if already at last log entry

Version 7x-0.3.0:

- support filters in navigation options

Version 7x-1.0.0:

- Bug fixes
